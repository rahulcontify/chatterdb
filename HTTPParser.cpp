//
// Created by rahul on 3/30/16.
//

#include "HTTPParser.h"

#include <cstring>
#include <iostream>
#include <sstream>

using namespace std;

HTTPParser::HTTPParser() :
        current_field(nullptr),
        header_complete(false),
        message_complete(false),
        is_header_state(true) {
    http_parser_init(&parser, HTTP_REQUEST);
    current_header.reserve(20); // TODO: Optimize this
    parser.data = this;
}

int HTTPParser::on_url(http_parser *parser, const char *at, size_t length) {
    HTTPParser *p = reinterpret_cast<HTTPParser *>(parser->data);
    p->url.append(at, length);
    return 0;
}

void HTTPParser::received_data(const char *data, std::size_t len) {
    if (header_complete) {
        throw InvalidData();
    }
    std::size_t nparsed = http_parser_execute(&parser, &settings, data, len);
    if (!parser.upgrade && nparsed != len) {
        throw ParserException(parser);
    }
}

int HTTPParser::on_status(http_parser *, const char *, std::size_t) {
    return 0;
}


int HTTPParser::on_header_field(http_parser *parser, const char *at, std::size_t length) {
    HTTPParser *p = reinterpret_cast<HTTPParser *>(parser->data);

    if(!p->is_header_state) {
        p->current_header.clear();
        p->current_field = nullptr;
        p->is_header_state = true;
    }
    p->current_header.append(at, length);

    return 0;
}

#define COMPARE_HDR(HDR) (p->current_header.size() == (sizeof(HDR) - 1) && ::strncasecmp(p->current_header.c_str(), HDR, (sizeof(HDR) - 1)) == 0)

int HTTPParser::on_header_value(http_parser *parser, const char *at, std::size_t length) {
    HTTPParser *p = reinterpret_cast<HTTPParser *>(parser->data);

    if (p->is_header_state) {
        if (COMPARE_HDR("User-Agent")) {
            p->current_field = &(p->user_agent);
        } else if (COMPARE_HDR("Upgrade")) {
            p->current_field = &(p->upgrade);
        } else if (COMPARE_HDR("Sec-WebSocket-Key")) {
            p->current_field = &(p->sec_websocket_key);
        } else if (COMPARE_HDR("Sec-WebSocket-Version")) {
            p->current_field = &(p->sec_websocket_version);
        }
        p->is_header_state = false;
    }

    if(p->current_field != nullptr) {
        p->current_field->append(at, length);
    }

    return 0;
}

http_parser_settings HTTPParser::settings = {HTTPParser::on_message_begin,
                                             HTTPParser::on_url,
                                             HTTPParser::on_status,
                                             HTTPParser::on_header_field,
                                             HTTPParser::on_header_value,
                                             HTTPParser::on_headers_complete,
                                             HTTPParser::on_body,
                                             HTTPParser::on_message_complete,
                                             nullptr, nullptr};

ParserException::ParserException(const http_parser& p) {
    enum http_errno err = HTTP_PARSER_ERRNO(&p);
    ostringstream oss;
    oss << http_errno_name(err) << ": " << http_errno_description(err);
    error = oss.str();
}

const char *ParserException::what() const noexcept {
    return error.c_str();
}

const char *InvalidData::what() const noexcept {
    return "Parser only handles header data";
}
