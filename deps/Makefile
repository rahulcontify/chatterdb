LLVM_RELEASE_VER=release_38
BOOST_RELEASE_VER=1.61.0
CLANG_C_COMPILER=/bin/clang
CLANG_CXX_COMPILER=/bin/clang++
MAKE_JOBS=1
ROOT_DIR=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SAN_TYPES := NoSan Address Undefined MemoryWithOrigins Thread
ALLSANTARGETS :=
GTEST_TARGETS := 
BOOST_TARGETS :=
HTTPPARSER_TARGETS :=

export PATH := $(ROOT_DIR)/llvm/install_local/bin:$(ROOT_DIR)/binutils/gold:$(ROOT_DIR)/binutils/binutils:$(PATH)

all: spdlog/CMakeLists.txt llvm/install_local/bin/clang++ gtesttarget boosttarget httpparsertarget jemalloc/install_local/lib/libjemalloc.a

spdlog/CMakeLists.txt:
	git clone --depth=1 --single-branch git@github.com:gabime/spdlog.git spdlog

llvm/CMakeLists.txt:
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/llvm.git llvm
	
llvm/tools/clang/CMakeLists.txt: llvm/CMakeLists.txt
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/clang.git llvm/tools/clang

llvm/tools/lldb/CMakeLists.txt: llvm/CMakeLists.txt
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/lldb.git llvm/tools/lldb

llvm/tools/lld/CMakeLists.txt: llvm/CMakeLists.txt
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/lld.git llvm/tools/lld

llvm/tools/clang/tools/clang-tools-extra/CMakeLists.txt: llvm/CMakeLists.txt
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/clang-tools-extra.git llvm/tools/clang/tools/clang-tools-extra

llvm/projects/compiler-rt/CMakeLists.txt: llvm/CMakeLists.txt
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/compiler-rt.git llvm/projects/compiler-rt
	
llvm/build_local: llvm/CMakeLists.txt
	mkdir -p llvm/build_local

llvm/install_local: llvm/CMakeLists.txt
	mkdir -p llvm/install_local
	
llvm/install_local/bin/clang++: llvm/CMakeLists.txt llvm/tools/clang/CMakeLists.txt llvm/tools/lldb/CMakeLists.txt llvm/tools/lld/CMakeLists.txt llvm/tools/clang/tools/clang-tools-extra/CMakeLists.txt llvm/projects/compiler-rt/CMakeLists.txt llvm/build_local llvm/install_local /usr/include/editline/readline.h binutils/gold/ld
	([ -f "$(ROOT_DIR)/llvm/build_local/CMakeCache.txt" ] || \
		(\
			cd $(ROOT_DIR)/llvm/build_local && \
			cmake -G "Unix Makefiles" \
				-DCMAKE_INSTALL_PREFIX=$(ROOT_DIR)/llvm/install_local \
				-DCMAKE_C_COMPILER=$(CLANG_C_COMPILER) \
				-DCMAKE_CXX_COMPILER=$(CLANG_CXX_COMPILER) \
				-DLLVM_TOOL_LLDB_BUILD=ON \
				-DLLVM_TOOL_LLD_BUILD=ON \
				-DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
				-DLLVM_INSTALL_UTILS=ON \
				-DLLVM_TOOL_CLANG_TOOLS_EXTRA_BUILD=ON \
				-DCMAKE_BUILD_TYPE=Release \
				-DLLVM_BINUTILS_INCDIR=$(ROOT_DIR)/binutils/include \
				-DLLVM_ENABLE_RTTI=ON ../ \
		)\
	) && ([ -f "$(ROOT_DIR)/llvm/install_local/bin/clang++" ] || \
		(\
			mkdir -p $(ROOT_DIR)/llvm/build_local/lib/python2.7 && \
			make -C $(ROOT_DIR)/llvm/build_local -j$(MAKE_JOBS) install \
	))

binutils/configure: llvm/install_local/bin/clang++
	git clone --depth=1 --single-branch git@github.com:bminor/binutils-gdb.git binutils

binutils/install_local: binutils/configure
	mkdir -p binutils/install_local

binutils/gold/ld-new: binutils/install_local
	cd binutils && \
	CC=$(CLANG_C_COMPILER) CXX=$(CLANG_CXX_COMPILER) CFLAGS="-march=native -mtune=native -O3" CXXFLAGS="-march=native -mtune=native -O3" ./configure \
	    --enable-gold --enable-plugins --disable-werror \
	    --prefix=$(ROOT_DIR)/binutils/install_local && \
	make all-gold && \
	make

binutils/gold/ld: binutils/gold/ld-new
	ln -s $(ROOT_DIR)/binutils/gold/ld-new $(ROOT_DIR)/binutils/gold/ld && \
	ln -s $(ROOT_DIR)/binutils/binutils/nm-new $(ROOT_DIR)/binutils/binutils/nm && \
	ln -s $(ROOT_DIR)/binutils/binutils/strip-new $(ROOT_DIR)/binutils/binutils/strip

jemalloc/configure.ac: llvm/install_local/bin/clang++
	[ -f "jemalloc/configure.ac" ] || \
	git clone --depth=1 --single-branch git@github.com:jemalloc/jemalloc.git jemalloc

jemalloc/install_local: jemalloc/configure.ac
	mkdir -p jemalloc/install_local

jemalloc/install_local/lib/libjemalloc.a: jemalloc/install_local
	cd jemalloc && \
	./autogen.sh && \
	CC=$(ROOT_DIR)/llvm/install_local/bin/clang CFLAGS="-march=native -mtune=native -g -O3 -funroll-loops" ./configure --prefix=$(ROOT_DIR)/jemalloc/install_local && \
	make dist && \
	make install

libcxx/CMakeLists.txt:
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/libcxx.git libcxx
	
libcxxabi/CMakeLists.txt:
	git clone --depth=1 -b $(LLVM_RELEASE_VER) --single-branch git@github.com:llvm-mirror/libcxxabi.git libcxxabi

gtest/CMakeLists.txt:
	git clone --depth=1 --single-branch git@github.com:google/googletest.git gtest

boost/bootstrap.sh:
	[ -f "boost/bootstrap.sh" ] || \
	(\
		rm -f boost_$(subst .,_,$(BOOST_RELEASE_VER)).tar.bz2 && \
		wget http://downloads.sourceforge.net/project/boost/boost/$(BOOST_RELEASE_VER)/boost_$(subst .,_,$(BOOST_RELEASE_VER)).tar.bz2 && \
		tar -xf boost_$(subst .,_,$(BOOST_RELEASE_VER)).tar.bz2 && \
		rm -rf boost && \
		mv boost_$(subst .,_,$(BOOST_RELEASE_VER)) boost \
	)

http-parser/Makefile:
	git clone --depth=1 --single-branch git@github.com:nodejs/http-parser.git http-parser

boost/project-config.jam: boost/bootstrap.sh
	cd boost && ./bootstrap.sh
	
libcxx/build_local: libcxx/CMakeLists.txt
	mkdir -p libcxx/build_local

libcxx/install_local: libcxx/CMakeLists.txt
	mkdir -p libcxx/install_local
	
libcxxabi/build_local: libcxxabi/CMakeLists.txt
	mkdir -p libcxxabi/build_local

libcxxabi/install_local: libcxxabi/CMakeLists.txt
	mkdir -p libcxxabi/install_local

gtest/build_local: gtest/CMakeLists.txt
	mkdir -p gtest/build_local

gtest/install_local: gtest/CMakeLists.txt
	mkdir -p gtest/install_local

boost/build_local: boost/bootstrap.sh
	mkdir -p boost/build_local

boost/install_local: boost/bootstrap.sh
	mkdir -p boost/install_local

http-parser/build_local: http-parser/Makefile
	mkdir -p boost/build_local

http-parser/install_local: http-parser/Makefile
	mkdir -p http-parser/install_local


define EVALFLAGS
BASE_COMPILE_FLAGS_$(SAN_TYPE) = -nostdinc++ -isystem$(ROOT_DIR)/libcxx/install_local/$(SAN_TYPE)/include -isystem$(ROOT_DIR)/libcxx/install_local/$(SAN_TYPE)/include/c++/v1
BASE_LINK_FLAGS_$(SAN_TYPE) = -L$(ROOT_DIR)/libcxx/install_local/$(SAN_TYPE)/lib -L$(ROOT_DIR)/libcxxabi/install_local/$(SAN_TYPE)/lib -stdlib=libc++ -lc++ -lc++abi
endef
$(foreach SAN_TYPE,$(SAN_TYPES),$(eval $(EVALFLAGS)))


COMPILE_FLAGS_Address = $(BASE_COMPILE_FLAGS_Address) -fsanitize=address -fno-omit-frame-pointer -O1 -g -fno-optimize-sibling-calls
LINK_FLAGS_Address = $(BASE_LINK_FLAGS_Address) -fsanitize=address

COMPILE_FLAGS_Undefined = $(BASE_COMPILE_FLAGS_Undefined) -fsanitize=undefined
LINK_FLAGS_Undefined = $(BASE_LINK_FLAGS_Undefined) -fsanitize=undefined

COMPILE_FLAGS_MemoryWithOrigins = $(BASE_COMPILE_FLAGS_MemoryWithOrigins) -fsanitize=memory -fsanitize-memory-track-origins=2 -fno-omit-frame-pointer -g -O1
LINK_FLAGS_MemoryWithOrigins = $(BASE_LINK_FLAGS_MemoryWithOrigins) -fsanitize=memory

COMPILE_FLAGS_Thread = $(BASE_COMPILE_FLAGS_Thread) -fsanitize=thread -g -O1
LINK_FLAGS_Thread = $(BASE_LINK_FLAGS_Thread) -fsanitize=thread -g -O1

COMPILE_FLAGS_NoSan = $(BASE_COMPILE_FLAGS_NoSan)
LINK_FLAGS_NoSan = $(BASE_LINK_FLAGS_NoSan)



define SANBUILDS

libcxxabi/install_local/$(SAN_TYPE)/lib/libc++abi.a : libcxxabi/build_local libcxxabi/install_local libcxxabi/CMakeLists.txt libcxx/CMakeLists.txt
	[ -f "libcxxabi/install_local/$(SAN_TYPE)/lib/libc++abi.a" ] || \
		(\
			mkdir -p libcxxabi/build_local/$(SAN_TYPE) && \
			cd libcxxabi/build_local/$(SAN_TYPE) && \
			cmake \
				-DLLVM_CONFIG=$(ROOT_DIR)/llvm/install_local/bin/llvm-config \
				-DLIBCXXABI_LIBCXX_INCLUDES=$(ROOT_DIR)/libcxx/include \
				-DLIBCXXABI_ENABLE_SHARED=OFF \
				-DLIBCXXABI_ENABLE_STATIC=ON \
				$(if $(filter-out NoSan, $(SAN_TYPE)),-DLLVM_USE_SANITIZER=$(SAN_TYPE)) \
				-DCMAKE_INSTALL_PREFIX=$(ROOT_DIR)/libcxxabi/install_local/$(SAN_TYPE) \
				-DCMAKE_C_COMPILER=$(ROOT_DIR)/llvm/install_local/bin/clang \
				-DCMAKE_CXX_COMPILER=$(ROOT_DIR)/llvm/install_local/bin/clang++ \
				$(ROOT_DIR)/libcxxabi && \
			make -j$(MAKE_JOBS) install \
		)
	
ALLSANTARGETS += libcxx/install_local/$(SAN_TYPE)/lib/libc++.a
libcxx/install_local/$(SAN_TYPE)/lib/libc++.a : libcxxabi/install_local/$(SAN_TYPE)/lib/libc++abi.a libcxx/CMakeLists.txt libcxxabi/CMakeLists.txt libcxx/build_local libcxx/install_local
	[ -f "libcxx/install_local/$(SAN_TYPE)/lib/libc++.a" ] || \
		(\
			mkdir -p libcxx/build_local/$(SAN_TYPE) && \
			cd libcxx/build_local/$(SAN_TYPE) && \
			cmake \
				-DLLVM_CONFIG=$(ROOT_DIR)/llvm/install_local/bin/llvm-config \
				$(if $(filter-out NoSan, $(SAN_TYPE)),-DCMAKE_BUILD_TYPE=RelWithDebInfo,-DCMAKE_BUILD_TYPE=Release) \
				-DLIBCXX_CXX_ABI_LIBRARY_PATH=$(ROOT_DIR)/libcxxabi/install_local/$(SAN_TYPE)/lib \
				-DLIBCXX_CXX_ABI=libcxxabi \
				-DLIBCXX_CXX_ABI_INCLUDE_PATHS=$(ROOT_DIR)/libcxxabi/include \
				-DLIBCXX_ENABLE_STATIC_ABI_LIBRARY=ON \
				-DLIBCXX_ENABLE_SHARED=OFF \
				-DLIBCXX_ENABLE_ABI_LINKER_SCRIPT=OFF \
				$(if $(filter-out NoSan, $(SAN_TYPE)),-DLIBCXX_ENABLE_ASSERTIONS=ON,-DLIBCXX_ENABLE_ASSERTIONS=OFF) \
				-DLIBCXX_ENABLE_ASSERTIONS=OFF \
				$(if $(filter-out NoSan, $(SAN_TYPE)),-DLLVM_USE_SANITIZER=$(SAN_TYPE)) \
				-DCMAKE_INSTALL_PREFIX=$(ROOT_DIR)/libcxx/install_local/$(SAN_TYPE) \
				-DCMAKE_C_COMPILER=$(ROOT_DIR)/llvm/install_local/bin/clang \
				-DCMAKE_CXX_COMPILER=$(ROOT_DIR)/llvm/install_local/bin/clang++ \
				$(ROOT_DIR)/libcxx && \
			make -j$(MAKE_JOBS) install \
		)

GTEST_TARGETS += gtest/install_local/$(SAN_TYPE)/lib/libgtest.a
gtest/install_local/$(SAN_TYPE)/lib/libgtest.a: libcxx/install_local/$(SAN_TYPE)/lib/libc++.a libcxxabi/install_local/$(SAN_TYPE)/lib/libc++abi.a gtest/build_local gtest/install_local
	[ -f "gtest/install_local/$(SAN_TYPE)/lib/libgtest.a" ] || \
		(\
			mkdir -p gtest/build_local/$(SAN_TYPE) && \
			cd gtest/build_local/$(SAN_TYPE) && \
			cmake \
				-DCMAKE_BUILD_TYPE=Release \
				-DCMAKE_C_FLAGS="$$(COMPILE_FLAGS_$(SAN_TYPE))" \
				-DCMAKE_CXX_FLAGS="$$(COMPILE_FLAGS_$(SAN_TYPE))" \
				-DCMAKE_INSTALL_PREFIX=$(ROOT_DIR)/gtest/install_local/$(SAN_TYPE) \
				-DCMAKE_C_COMPILER=$(ROOT_DIR)/llvm/install_local/bin/clang \
				-DCMAKE_CXX_COMPILER=$(ROOT_DIR)/llvm/install_local/bin/clang++ \
				$(ROOT_DIR)/gtest && \
			VERBOSE=1 make -j$(MAKE_JOBS) install \
		)

BOOST_TARGETS += boost/install_local/$(SAN_TYPE)/lib/libboost_system.a

boost/install_local/$(SAN_TYPE)/lib/libboost_system.a: boost/project-config.jam boost/install_local boost/build_local
	[ -f "boost/install_local/$(SAN_TYPE)/lib/libboost_system.a" ] || \
		(\
			mkdir -p boost/build_local/$(SAN_TYPE) && \
			cd boost && \
			PATH="$(ROOT_DIR)/llvm/install_local/bin/:$${PATH}" ./b2 -j$(MAKE_JOBS) -q -d+2 \
				toolset=clang \
				link=static \
				cxxflags="$$(COMPILE_FLAGS_$(SAN_TYPE))" \
				linkflags="$$(LINK_FLAGS_$(SAN_TYPE))" \
				--prefix="$(ROOT_DIR)/boost/install_local/$(SAN_TYPE)"  \
				--build-dir="$(ROOT_DIR)/boost/build_local/$(SAN_TYPE)" \
				install \
		)

HTTPPARSER_TARGETS += http-parser/install_local/$(SAN_TYPE)/lib/libhttp_parser.a
http-parser/install_local/$(SAN_TYPE)/lib/libhttp_parser.a: http-parser/Makefile http-parser/build_local http-parser/install_local
	[ -f "http-parser/install_local/$(SAN_TYPE)/lib/libhttp_parser.a" ] || \
		(\
			cd http-parser && \
			make clean && \
			make \
				CC=$(ROOT_DIR)/llvm/install_local/bin/clang \
				CPPFLAGS="$$(COMPILE_FLAGS_$(SAN_TYPE))" \
				CFLAGS="$$(COMPILE_FLAGS_$(SAN_TYPE))" \
				LDFLAGS="$$(LINK_FLAGS_$(SAN_TYPE))" \
				PREFIX="$(ROOT_DIR)/http-parser/install_local/$(SAN_TYPE)"  \
				package && \
			mkdir -p $(ROOT_DIR)/http-parser/install_local/$(SAN_TYPE)/lib && \
			cp libhttp_parser.a $(ROOT_DIR)/http-parser/install_local/$(SAN_TYPE)/lib/ \
		)		

endef

$(foreach SAN_TYPE,$(SAN_TYPES),$(eval $(SANBUILDS)))

.PHONY: santarget
santarget: $(ALLSANTARGETS)	

.PHONY: gtesttarget
gtesttarget: $(GTEST_TARGETS)

.PHONY: boosttarget
boosttarget: $(BOOST_TARGETS)	

.PHONY: httpparsertarget
httpparsertarget: $(HTTPPARSER_TARGETS)


