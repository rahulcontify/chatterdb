To test wsserver

wstest -m fuzzingclient

profiling
valgrind --tool=callgrind --dump-instr=yes --collect-jumps=yes ./bbchatter > /dev/null

Dependencies:
makeinfo
texinfo
flex

Increase file descripter limit
http://www.cyberciti.biz/faq/linux-increase-the-maximum-number-of-open-files/